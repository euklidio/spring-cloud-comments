package com.epam.user.controller;

import com.epam.user.dto.PasswordDto;
import com.epam.user.security.UserPrincipal;
import com.epam.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {

    @Autowired
    UserService service;

    @RequestMapping(value = "/change-password", method = RequestMethod.PUT)
    @ResponseBody
    public void changePassword(@RequestBody PasswordDto passwordDto) {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        service.changePassword(userPrincipal.getUser().getId(), passwordDto);
    }

}
