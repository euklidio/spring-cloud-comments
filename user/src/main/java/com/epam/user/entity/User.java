package com.epam.user.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document
@Data
@Accessors(chain = true)
public class User {

    @Id
    private UUID id;
    private String email;
    private String firstName;
    private String lastName;
    private String password;

    public User() {
        this.id = UUID.randomUUID();
    }
}
