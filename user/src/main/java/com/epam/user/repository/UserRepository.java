package com.epam.user.repository;

import com.epam.user.entity.User;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource
@EnableFeignClients
public interface UserRepository extends MongoRepository<User, UUID> {

    User findByEmail(@Param("email") String email);
}
