package com.epam.user.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Accessors(chain = true)
public class PasswordDto {
    String oldPassword;
    String newPassword;
}
