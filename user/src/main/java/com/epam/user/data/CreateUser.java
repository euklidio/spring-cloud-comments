package com.epam.user.data;

import com.epam.user.entity.User;
import com.epam.user.repository.UserEventHandler;
import com.epam.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class CreateUser {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    public void init() {
        User initUser = userRepository.findByEmail("test@test.epam.com");
        if (initUser != null) {
            return;
        }

        User user = new User()
                .setEmail("test@test.epam.com")
                .setPassword(passwordEncoder.encode("test$123"))
                .setFirstName("John")
                .setLastName("Novak");

        userRepository.save(user);
    }
}
