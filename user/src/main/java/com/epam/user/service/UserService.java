package com.epam.user.service;

import com.epam.user.dto.PasswordDto;
import com.epam.user.entity.User;
import com.epam.user.exception.IncorrectPasswordException;
import com.epam.user.exception.NotFoundException;
import com.epam.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserService {

    @Autowired
    UserRepository repository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public void changePassword(UUID userId, PasswordDto passwordDto) {
        User user = repository.findOne(userId);

        if (user == null) {
            throw new NotFoundException("There is not user with id: " + userId);
        }

        if (!passwordEncoder.matches(passwordDto.getOldPassword(), user.getPassword())) {
            throw new IncorrectPasswordException();
        }

        user.setPassword(passwordEncoder.encode(passwordDto.getNewPassword()));

        repository.save(user);
    }
}
