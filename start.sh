#!/usr/bin/env bash

cd eureka
mvn clean install -Dmaven.test.skip=true
cd ../config/
mvn clean install -Dmaven.test.skip=true
cd ../user
mvn clean install -Dmaven.test.skip=true
cd ../comment/
mvn clean install -Dmaven.test.skip=true
cd ../gateway/
mvn clean install -Dmaven.test.skip=true

docker-compose down
docker-compose up --force-recreate -d