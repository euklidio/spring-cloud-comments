package com.epam.comment.service;

import com.epam.comment.dto.CommentDto;
import com.epam.comment.entity.Comment;
import com.epam.comment.entity.Group;
import com.epam.comment.exception.NotFoundException;
import com.epam.comment.mapper.CommentMapper;
import com.epam.comment.repository.GroupRepository;
import com.epam.comment.user.UserClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;


@Component
public class CommentService {

    Logger logger = LoggerFactory.getLogger(CommentService.class);

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private UserClient userClient;


    public CommentDto createComment(UUID groupId, Comment newComment) {
        Group group = getGroup(groupId);

        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        newComment.setUserId(userClient.getUserByEmail(email).getId());

        group.addComment(newComment);
        groupRepository.save(group);

        logger.info("Created comment: " + newComment);

        return commentMapper.mapToDto(newComment);
    }

    public CommentDto updateComment(UUID groupId, UUID commentId, Comment newComment) {
        Group group = getGroup(groupId);
        Comment commentToUpdate = groupRepository.findComment(group, commentId);

        updateValues(commentToUpdate, newComment);
        logger.info("Updated comment: " + commentToUpdate);
        groupRepository.save(group);

        return commentMapper.mapToDto(commentToUpdate);
    }

    public void deleteComment(UUID groupId, UUID commentId) {
        Group group = getGroup(groupId);
        Comment commentToDelete = groupRepository.findComment(group, commentId);

        group.removeComment(commentToDelete);

        logger.info("Removed comment: " + commentToDelete);
        groupRepository.save(group);
    }

    private Group getGroup(UUID groupId) {
        Group group = groupRepository.findOne(groupId);

        if (group == null)
            throw new NotFoundException("There is no group for id: " + groupId);

        return group;
    }

    private void updateValues(Comment toUpdate, Comment newComment) {
        toUpdate.setDateTime(LocalDateTime.now())
                .setText(newComment.getText())
                .setUserId(newComment.getUserId());
    }
}
