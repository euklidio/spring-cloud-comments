package com.epam.comment.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Document
@Data
@Accessors(chain = true)
public class GroupDto {
    private UUID id;
    private List<CommentDto> comments = new ArrayList<>();

}
