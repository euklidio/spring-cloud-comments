package com.epam.comment.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class UserDto {
    private UUID id;
    private String email;
    private String firstName;
    private String lastName;
    private String password;
}
