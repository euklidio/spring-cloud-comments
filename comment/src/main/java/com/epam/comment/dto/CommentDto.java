package com.epam.comment.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Document
@Data
@Accessors(chain = true)
public class CommentDto {
    @Id
    private UUID id;
    private UserDto user;
    private LocalDateTime dateTime;
    private String text;
}
