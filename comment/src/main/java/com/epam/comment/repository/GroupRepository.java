package com.epam.comment.repository;

import com.epam.comment.entity.Comment;
import com.epam.comment.entity.Group;
import com.epam.comment.exception.NotFoundException;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface GroupRepository extends MongoRepository<Group, UUID> {
    default Comment findComment(Group group, UUID commentId) {
        return group.getComments().stream().filter(c -> c.getId().equals(commentId)).findFirst().orElseThrow(() ->
                new NotFoundException("There is no comment id for: " + commentId));
    }
}
