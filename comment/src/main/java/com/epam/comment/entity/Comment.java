package com.epam.comment.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Document
@Data
@Accessors(chain = true)
public class Comment {
    @Id
    private UUID id;
    private UUID userId;
    private LocalDateTime dateTime;
    private String text;

    public Comment() {
        id = UUID.randomUUID();
        dateTime = LocalDateTime.now();
    }
}
