package com.epam.comment.entity;


import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Document
@Data
@Accessors(chain = true)
public class Group {

    @Id
    private UUID id;

    private List<Comment> comments = new ArrayList<>();

    public Group() {
        this.id = UUID.randomUUID();
    }

    public void addComment(Comment comment) {
        comments.add(comment);
    }

    public void removeComment(Comment comment) {
        comments.remove(comment);
    }
}
