package com.epam.comment.mapper;

import com.epam.comment.dto.GroupDto;
import com.epam.comment.entity.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Scope("prototype")
@Component
public class GroupMapper {

    @Autowired
    CommentMapper commentMapper;

    public GroupDto mapToDto(Group group) {
        return new GroupDto()
                .setId(group.getId())
                .setComments(
                        group.getComments()
                                .stream()
                                .map(commentMapper::mapToDto)
                                .collect(Collectors.toList())
                );
    }
}
