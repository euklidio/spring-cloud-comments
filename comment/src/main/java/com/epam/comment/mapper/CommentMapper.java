package com.epam.comment.mapper;

import com.epam.comment.dto.CommentDto;
import com.epam.comment.entity.Comment;
import com.epam.comment.user.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class CommentMapper {

    @Autowired
    UserClient userClient;

    public CommentDto mapToDto(Comment comment) {
        CommentDto commentDto = new CommentDto()
                .setId(comment.getId())
                .setText(comment.getText())
                .setDateTime(comment.getDateTime());

        if (comment.getUserId() != null) {
            commentDto.setUser(userClient.getUser(comment.getUserId()));
        }

        return commentDto;
    }
}
