package com.epam.comment.controller;

import com.epam.comment.dto.CommentDto;
import com.epam.comment.entity.Comment;
import com.epam.comment.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("{groupId}/comments")
public class CommentController {

    @Autowired
    private CommentService commentService;


    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public CommentDto create(@PathVariable UUID groupId, @RequestBody Comment comment) {
        return commentService.createComment(groupId, comment);
    }

    @RequestMapping(value = "{commentId}", method = RequestMethod.PUT)
    @ResponseBody
    public CommentDto update(@PathVariable UUID groupId, @PathVariable UUID commentId, @RequestBody Comment
            newComment) {
        return commentService.updateComment(groupId, commentId, newComment);
    }

    @RequestMapping(value = "{commentId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable UUID groupId, @PathVariable UUID commentId) {
        commentService.deleteComment(groupId, commentId);
    }
}
