package com.epam.comment.controller;

import com.epam.comment.dto.GroupDto;
import com.epam.comment.entity.Group;
import com.epam.comment.mapper.GroupMapper;
import com.epam.comment.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class GroupController {

    @Autowired
    private GroupRepository repository;

    @Autowired
    private GroupMapper groupMapper;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public List<GroupDto> getAll() {
        return repository.findAll().stream().map(groupMapper::mapToDto).collect(Collectors.toList());
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public GroupDto get(@PathVariable UUID id) {
        return groupMapper.mapToDto(repository.findOne(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public GroupDto create() {
        return groupMapper.mapToDto(repository.save(new Group()));
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable UUID id) {
        repository.delete(id);
    }
}
