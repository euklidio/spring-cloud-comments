package com.epam.comment.user;

import com.epam.comment.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Component
public class UserClient {

    @Autowired
    UserFeignClient userClient;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public UserDto getUser(@PathVariable("id") UUID id) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        return userClient.getUser(id, request.getHeader("Authorization"));
    }

    @RequestMapping(value = "/users/search/findByEmail", method = RequestMethod.GET)
    public UserDto getUserByEmail(@Param("email") String email){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        return userClient.getUserByEmail(email, request.getHeader("Authorization"));
    }
}
