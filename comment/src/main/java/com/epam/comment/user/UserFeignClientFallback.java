package com.epam.comment.user;

import com.epam.comment.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserFeignClientFallback implements UserFeignClient {
    @Override public UserDto getUser(UUID id, String token) {
        return null;
    }

    @Override public UserDto getUserByEmail(String email, String token) {
        return null;
    }
}
