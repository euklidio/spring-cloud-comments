package com.epam.comment.user;

import com.epam.comment.dto.UserDto;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@FeignClient(value = "user-service", fallback = UserFeignClientFallback.class)
@Primary
public interface UserFeignClient {
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    UserDto getUser(@PathVariable("id") UUID id, @RequestHeader("Authorization") String token);

    @RequestMapping(value = "/users/search/findByEmail", method = RequestMethod.GET)
    UserDto getUserByEmail(@RequestParam("email") String email, @RequestHeader("Authorization") String token);
}
